## Une nouvelle communauté

Lieu de mise à jour du site de cette nouvelle communauté qui se développe entre Pont-Aven, Port Manec'h et Stang Tremorvezen

<mark>Attention à l'intégration continue! Je n'ai pas encore de script qui permette de faire un build sur GitLab. Il faut donc construire le site avant de le pousser sur le master pour qu'il puisse être mis en ligne.</mark>

Le site nouvelle-communaute.gitlab.io est en ligne grâce à l'aide précieuse d'[Eleventy](https://www.11ty.dev/).