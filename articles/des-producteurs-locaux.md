---
pageTitle: Des producteurs locaux
date: Last Modified
---
Ci-dessous la liste des producteurs locaux à compléter et à tester. Les producteurs sont qualifiés de locaux quand il s'agit bien de production, et non uniquement de transformation, et quand le lieu de production ou le lieu de distribution directe se situe à une distance égale ou inférieure au [rayon d'action de la communauté](/articles/le-rayon-d-action-de-la-communaute/). Pensez à proposer aux différents producteurs de renseigner le site [mangeons-local.bzh/](https://www.mangeons-local.bzh/).

## La Petite Ferme de Kercaudan - Arthur Capdeville
Tél. 07 82 10 34 82 - Légumes bio et cidre - Ouverture : à la ferme le mercredi de 14h à 18h et vente sur les marchés : mardi à Pont-Aven, samedi à Névez.

## Minoterie Le Dérout — Farines, farines biologiques. Vente en directe aux particuliers.
(Attention Transformation) 10 rue du Bois d’Amour - 29930 Pont-Aven - Tél : 02 98 06 02 03 - Vente aux particuliers, farine et paillage. • Ouverture : du lundi au vendredi sauf mercredi après-midi

## Le Safran des Hermines — Olivier Gourmelen
Lieu-dit Kernaour 29930 Pont-Aven Tél : 06 89 33 76 39 o.gourmelen29@gmail.com Safran des Hermines Safran, huile safranée, miel, bonbons, sirop de safran…. Vente sur place. Récolte en octobre. • Ouverture : toute l’année, le mercredi de 13h30 à 20h30 et sur rendez-vous

## TERR’AVEN - Miel en rayon, safran, vinaigre de cidre, sel aux aromates...
Une petite ferme naturelle : [Les produits](https://www.terraven.fr/nos-produits)

## La ferme de Bossulan — Terre et Paille — Alain et Philippe Sinquin Bossulan
Bossulan, 29930 Pont-Aven - 06 24 15 05 36 - philippesinquin@orange.fr
Charcuterie et viande fraiche de porcs élevés sur paille, nourriture à base de céréales garanties sans OGM produites sur l’exploitation. Livraison vers Paris et Nantes tous les mois. • Ouverture : chaque samedi de 11h à 12h30 et livraison possible. [Site www](http://terreetpaille.fr)

## SARL Le Naour Jean-Marie Le Naour Saint-Maudé
29930 Pont-Aven Tél : 06 78 18 35 98 labyrinthedepontaven@gmail.com Viande bovine blonde d’aquitaine élevée Sans OGM • Ouverture : vendredi soir et samedi matin selon commandes (1fois/mois)

## La Basse-Cour Maude Rousseau Kercaudan
29930 Pont-Aven Tél : 02 98 71 51 30 ou 06 24 15 05 36 Toute l’année : poulets, pintades, canards, cailles, lapins, cochons. Et à Noël : dindes, oies, chapons, pintades chaponnées, faisans. • Ouverture : à la ferme le vendredi de 17h à 19 h et vente sur les marchés.

## Le potager de saint'é
Trégunc 29910 Tel : 02 98 50 29 81 e-mail: phcamama@yahoo.fr

## Ferme de Kersegalou — Produits laitiers et viande de la ferme
Située entre l’Aven et le Bélon, sur la commune de Riec-sur-Bélon, la ferme abrite un troupeau d’environ 85 vaches laitières et leur suite, auprès desquelles s’activent Vincent, Annie, Louis-Pierre et Odile. Vente à la ferme : Nos produits laitiers sont vendus à la ferme le vendredi de 16 à 19 h, ou en matinée sur 2 marchés : Trégunc le mercredi et Riec le samedi — [Site www](https://kersegalou.wordpress.com/).

## Les ruchers Marneffe & compagnie - Miel d'été et miel de printemps extrait et conditionnés sous vos yeux
Rozic, 29380 Le Trévoux - Téléphone : 0646267623 - geoffroy.marneffe@laposte.net. Vente de 16h à 19h à notre magasin le mardi et le vendredi.

## bateau de pêche Zeus Faber — Petite pêche et vente directe sur Tregunc
Cale de Porz an Halen 29910 Tregunc (Attention Hors-jeu - 12km à partir du point le plus proche - Pont-Aven !) — Fred, 27 ans, va seul en mer tous les matins depuis fin 2017. Sur son ligneur polyvalent, le Zeus Faber, il pratique la petite pêche (pêche du jour, bateau de moins de 10 mètres et pêchant dans la zone des 6 miles). Gage de fraîcheur, les poissons et crustacés du jour sont proposés en vente directe. Fred propose aux consommateurs du poisson frais, de saison, sans intermédiaire et à prix fixe toute l’année. Selon la saison et pour les poissons, vous retrouverez des rougets, lottes, tacauds, merlans, etc. Pour les crustacés, des homards, langoustes, araignées et étrilles pêchés aux casiers. Retrouvez les prix sur [le site www](https://vente-au-bateau-zeus-faber-53.webself.net/).

## La Ferme de Kercabon Vras - Légumes, miels, oeufs. Paniers de légumes à récupérer lors de la vente à la ferme.
Kercabon Vras - 29380 - BANNALEC - Tél : 06 81 03 96 63 - silvent.robin@neuf.fr - Légumes, miels, oeufs. Paniers de légumes à récupérer lors de la vente à la ferme. Autres produits dans le panier: jus de pomme, œufs, soupes
Jours d'ouverture le vendredi de 16h30 à 19h30. Retrouvez aussi ces produits : AMAP de Riec-sur-Belon.

## AMAP de Riec-sur-Belon
Les producteurs de l’amap viennent livrer leurs produits le mercredi soir de 18h15 à 19h30 à la salle Ty Forn (4 rue du presbytère) à Riec-sur-Belon. Producteurs: Robin Silvent – légumes - Les jardins bio du Belon – légumes - La ferme de Kerségalou – produits laitiers - Bara’laezh – pains - Breizh pommes – pommes et jus de pommes - Saveurs en chemin – gelée de fleurs, pesto - Ferme de Kimerc’h – fromage de vache - Baume Shanti – produits de soin à base de plantes médicinales - Laura et ses chèvres – fromage de chèvre - Olivier Blondin – miel - Bio Pizza – pizzas - La poule mouillée – poulets bio - Peau de vache – savons - Ty Jaouen – Bières Artisanales - Farfad’algues – tartares d’algues - Bruno Thomas – viande d’agneau, oeufs, kiwi

D'autres produits et producteurs sur [Bon Plan Bio](http://bonplanbio.fr/).
