---
pageTitle: Le rayon d'action de la communauté
date: Last Modified
---
Fort d'une stratégie bas carbone, la communauté va privilégier le vélo classique (ce sont les plus anciens qui sont contents). Pour des déplacements quotidiens, nous pouvons penser parcourir des trajets de 10 km aller — soit environ 30 mn de vélo — et même chose pour le retour. Cette estimation reste à être validée par un peu de pratique sur les routes, chemins et dénivelés. Ceux qui ont déjà testé peuvent nous éclairer. Des tests devront aussi être menés avec [charette](/articles/une-charette-pour-les-victuailles/) pleine de victuailles.

## De Pont-Aven à Port Manec'h
Pour aller de la famille Carnoy à la famille Rondeau du Noyer, il y a 9,2 km à faire. Pour se rendre chez les Degrange, il faut pédaler 9,4 km depuis Pont-Aven.

![légende](/iconographie/carte-pa-pm-001.gif)

## Faire nos courses à Ty Vrac
Bonne nouvelle ! L'épicerie zero dechet bio [Ty vrac](https://paulinedouguet.wixsite.com/website) située au 8 Place de l'Église à Trégunc, est à 8,8 km du coteau de Keramperchec et à 9,3 km depuis Stang. Les confinés de la rue de Kerambris, à 11 km de l'épicerie, devront faire faire leurs courses à d'autres.

![légende](/iconographie/carte-pa-tregunc-001.gif)

Les cartes sont pour le moment des copies d'écran du site [OpenStreetMap](https://www.openstreetmap.org/). il faut penser à les passer en SVG pour que le site diminue son empreinte.
