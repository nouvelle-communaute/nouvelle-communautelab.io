---
pageTitle: En route pour l'autonomie
date: Last Modified
---

Pour une autonomie totale, c'est à dire hors viande et poissons, les protéines étant apportées par les légumineuses), il faut un objectif de production d’un kilogramme de nourriture par jour et par personne.

En diversifiant les plantations axées sur sept sortes d’aliments à raison d’environ 50 kg de chaque, cela donnera alors 350kg (en arrondissant donc, les 365 kg dont on a besoin, chacun).

- 60 kg de céréales (blé, orge, seigle, maïs, avoine, etc…) cultivés sur 450m2 (rendements en culture non mécanisée, en bio) ou sur 200m2 (rendements en culture mécanisée bio) avec 1,5 kg de semences.
- 50 kg d’oléagineux (noix, noisettes, graines de courge, tournesol, pavot, colza, etc…) cultivés sur 400m2
- 50 kg de pommes de terre (+ autres tubercules, châtaignes, etc…) cultivés sur 20m2 avec une centaine de plants de pommes de terre p.ex. Donnée de base : rendement de pommes de terre : 2,5 kg / m2.
- 60 kg de fruits (pommes, poires, raisins, etc…) cultivés en vivaces sur environ 120m2.
- 50 kg de choux (divers y compris rutabagas, navets, etc…) cultivés sur environ 20m2.
- 50 kg de légumes (carottes, oignons, poireaux, céleris, etc…) cultivés sur environ 30m2.
- 30 kg de légumineuses (haricots, pois, (secs) etc…) cultivés sur 120m2 avec 2 à 3 kg de semences.

## Articles à lire et à résumer

[transitionautonomie.fr/autonomie-alimentaire-surface/](https://transitionautonomie.fr/autonomie-alimentaire-surface/)
