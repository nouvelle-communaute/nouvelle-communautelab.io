---
pageTitle: Déterminer le centre de ce petit monde
date: Last Modified
---
Comment déterminer le centre des 3 coordonnées ci-dessous - les points de réference de notre petite communauté ?

- 13 rue de keramperchec pont aven - latitude : 47.850213 et longitude : -3.748364
- 12 Stang Tremorvezen - 29920 Névez - 47.799109, -3.773607
- 5 Rue de Kerambris - 29920 Névez - 47.799125, -3.747060

Si nous faisons la somme des latitudes divisée par 3 et la somme des longitudes divisées par 3, nous obtenons un point dont les coordonnées GPS sont 47.816149, -3.756344 - soit pas loin du [fond de l'anse du Poulguin](https://www.openstreetmap.org/search?query=47.816149%2C%20-3.756344#map=15/47.8162/-3.7563&layers=G).

![Centre de la communauté - non loin de l'anse du Poulguin](/iconographie/carte-anse-poulguin-001.gif)

Mais est-ce bien le centre de notre triangle ? Trop loin pour les pont-avenistes, il faudrait calculer le point de rencontre idéal pour qu'un trajet depuis chacun des points ci-dessus soit le plus court possible à vélo. Une rapide recherche sur OpenStreetMap, nous indique que la cale de Kerdruc est un bon point de rencontre - à 5,8km de Pont-Aven, 5,5km de Stang et 5,5km de Port Manec'h.
