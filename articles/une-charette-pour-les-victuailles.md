---
pageTitle: Une charette pour les victuailles
date: Last Modified
---
Le vélo, c'est bien pour la planète mais encore faut-il pouvoir faire ses courses. Si le Vélo Cargo est une option, la charette est plus économique et aussi plus utile quand nous sommes à pied.

Nous pouvons commencer à rêver des [charettes Carry Freedom](https://carryfreedom.com/) mais nous pouvons surtout profiter de l'initiative de la firme allemande pour aider les pays en difficultés. Une remorque en bambou conçue pour être construite selon [des plans disponibles à tous](https://carryfreedom.com/bamboo-trailer/).

![Carry Freedom Bamboo Trailer](/iconographie/bamboo-trailer-001.gif)

Pyc construira une version avec un bras plus long pour pouvoir transporter le canoë.
