---
pageTitle: Un été 2020 riche en rencontres
date: Last Modified
---

Notes à ranger. Article à venir.

Le [wiki des Comices du Faire 2020](https://movilab.org/wiki/Comices_Du_Faire_2020) sur MovieLab.

Une [cartographie](https://miro.com/app/board/o9J_km2hQJ8=/) en cours des lieux, projets et rencontres.

Un article sur les comices du faire 2020 : [Aux Comices du Faire, le camp d’été breton des amoureux de la lowtech](https://www.makery.info/2019/08/17/aux-comices-du-faire-le-camp-dete-breton-des-amoureux-de-la-lowtech/)
