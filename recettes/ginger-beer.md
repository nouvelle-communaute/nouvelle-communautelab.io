---
pageTitle: Ginger Beer
date: Last Modified
---

---
- Gingembre rapé
- Sucre en poudre
- Eau non chlorée - Avoir toujours un broc avec de l'eau du robinet laissée à l'air depuis plus de 4 heures
- 1 pot en verre avec joint caoutchouc
- 1 bouteille en verre avec joint caoutchouc
---
## Ginger Bug
Ingrédient indispensable à la réalisation de votre Ginger Beer.

1. Mettre 1/2 litre d'eau non chlorée dans le pot en verre
1. Mettre 1 cuillerée à soupe de gingembre rapé
1. Mettre 1 cuillerée à soupe de sucre en poudre
1. Fermer hermétiquement le pot, le mettre près de votre table de cuisson (une chaleur un peu élevée va accélérer le travail des bactéries) et Attendre 24h
1. Le lendemain ajouter
1. Répéter cette même opération (gingembre et sucre) pendant 4 ou 5 jours. À chaque nouvelle ouverture de votre pot, vous devez sentir une pression toujours plus forte. C'est le signe que tout va bien.

## Ginger beer
<mark>Attention!</mark> Quand vous ouvrirez votre Ginger Beer pour la servir, la pression peut être assez forte et vous pouvez facilement arroser sol et murs!

1. Mettre 100g de sucre dans la bouteille
1. Ajouter le même volume de gingembre rapé
1. Ajouter 20cl de Ginger Bug filtré
1. Compléter avec de l'eau non chlorée pour atteindre 9/10 de la bouteille - attention de ne pas remplir la bouteille complétement!
1. Fermé la bouteille, la mettre au même endroit que votre Ginger Bug et attendre 12 ou 24h
1. Mettre au frais avant degustation - Encore une fois, attention au moment de servir!

![Ginger Beer](/iconographie/ginger-beer-001.gif)
