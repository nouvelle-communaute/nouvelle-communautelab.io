---
pageTitle: Crêpes sucrées
date: Last Modified
---

* 8 œufs
* 80g de sucre
* 400g de farine
* 1 litre de lait
* 60g de beurre fondu

À mélanger dans l'ordre ci-dessus
