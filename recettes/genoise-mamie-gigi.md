---
pageTitle: Génoise Mamie Gigi
date: Last Modified
---
---
- 6 œufs
- 200g de sucre
- 125g de farine
---
1. Mélanger les jaunes d'œufs et le sucre jusqu'à obtention d'un ruban
1. Monter les blancs en neige
1. Mélanger les jaunes sucrés avec 1/3 des blancs en neige
1. Rajouter la moitié de la farine
1. Rajouter 1/3 des blancs en neige
1. Rajouter le reste de la farine
1. Rajouter le reste des blancs en neige
1. Mettre dans un plat
1. Cuisson 35/45 min four à 180°C
---
