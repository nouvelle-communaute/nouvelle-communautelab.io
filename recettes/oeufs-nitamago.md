---
pageTitle: Œufs nitamago
date: Last Modified
---

---
La marinade:

- 8 oeufs bio à température ambiante
- 100 g de sauce soja
- 100 g de mirin
- 100 g de saké de cuisine
- 1 cuillère à soupe de miel d’acacia
- 1 gousse d’ail
- un morceau 3 cm de gingembre frais
- 1 pincée de poivre de Sichuan

1. Faire frémir 3 à 5 minutes soja + mirin + saké  + miel + ail + gingembre + poivre de Sichuan dans une casserole.
2. Faire bouillir une grande casserole d’eau & plonger les oeufs pendant 6 minutes. Stopper la cuisson en les plongeant dans un saladier d’eau glacée. Les écaler – attention opération fragile !
3. Versez la sauce refroidie dans une boite hermétique & plongez-y les oeufs entiers dedans. Laisser mariner 2 à 6 jours.

À servir en salade avec
- quelques graines de sésame
- quelques herbes fraiches de saison
- un peu de sauce pimentée Sriracha (facultatif)
- poivre du moulin
