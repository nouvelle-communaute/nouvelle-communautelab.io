---
pageTitle: Pâte brisée
date: Last Modified
---


---
- 200 g farine
- 100 g beurre salé fondu
- eau glacée  (1/3 de verre)
---
1. Mélanger rapidement directement dans un plat à tarte en terre (Moins de vaisselle!)
1. Étaler la pâte à la main avec le bout des doigts pour la faire monter sur les bords du plat
1. Laisser reposer 20 min
1. Garnir
1. Cuisson 35 min four à 180°
---
